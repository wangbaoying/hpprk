/**
 * 
 */
package com.bawei.csm_rk9.util;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @author hasee
 * @date 2020年12月18日
 *  @parameter DateUtil.java
 * @return 
 */
public class DateUtil {
	public static String  getStringFromDate (Date date) {
		SimpleDateFormat simpleFormat = new SimpleDateFormat("yyyy-MM-dd");
		return simpleFormat.format(date);
		}
	//根据传入的参数获取该日期的月初日期，例如给定的日期为“2019-09-19 19:29:39”，返回“2019-09-01 00:00:00”
	public static String getDateByMonthInit (Date src) {
		SimpleDateFormat simpleFormat = new SimpleDateFormat("2020-09-10");
		return simpleFormat.format(src);
		}
	//根据传入的参数获取该日器的月末日期，例如给定的日期为“2019-09-19 19:29:39”，返回“2019-09-30 23:59:59”，注意月大月小以及闰年
	public static Date getDateByMonthLast(Date date) {
		// 思路： 让当前日期的月份加1个月，然后获取月初，再减去1秒
				// 1.获取系统日期
				Calendar c = Calendar.getInstance();
				// 2.用传入的日期初始化日历类
				c.setTime(date);
				// 3让当前日期的月份加1个月
				c.add(Calendar.MONTH, 1);
				// 4.然后获取月初
				c.set(c.get(Calendar.YEAR), c.get(Calendar.MONTH), 1, 0, 0, 0);
				// 5.再减去1秒
				c.add(Calendar.SECOND, -1);
			
				return c.getTime();
		}
	//求未来日期离今天还剩的天数
	public static Date offsetDate(int days) {
		Calendar c = Calendar.getInstance();
		c.add(Calendar.DAY_OF_MONTH, days);
		return c.getTime();
	}
	//求过去日期离今天过去的天数
	// 获取当前时间的指定天数的时间(正数：未来日期；负数：历史日期)
		public static Date offsetDate1(int days) {
			Calendar c = Calendar.getInstance();
			c.add(Calendar.DAY_OF_MONTH, days);
			return c.getTime();
		}
}
