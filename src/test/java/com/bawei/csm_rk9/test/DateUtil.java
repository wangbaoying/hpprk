/**
 * 
 */
package com.bawei.csm_rk9.test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.junit.Test;

import com.carrey.carrey_utils.DateUtils;

/**
 * @author hasee
 * @date 2020年12月18日
 *  @parameter DateUtil.java
 * @return 
 * 封装一个工具类DateUtil,包含以下方法
 */
public class DateUtil {
	//根据传入的日期获取年龄
    @Test
    public void DateUtil1() throws ParseException{
    	   SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");
    	   Date date = sdf.parse("1998-12-16");
    	   int age = DateUtils.getAge(date);
    	   System.out.println(age);
    }
    //根据传入的参数获取该日期的月初日期，例如给定的日期为“2019-09-19 19:29:39”，返回“2019-09-01 00:00:00”
    @Test
    public void DateUtil2() throws ParseException{
       SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd");
    	  Date date=simpleDateFormat.parse("2019-09-01");
    	 System.out.println(date);
    }
    
    @Test
    public void DateUtil3() throws ParseException{
       SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd");
    	  Date date=simpleDateFormat.parse("2019-09-01");
    	      DateUtil.getDateByMonthLast(date);
    	      System.out.println(date);
    }
	/**
	 * @param date
	 */
	private static void getDateByMonthLast(Date date) {
		Object getDateByMonthLast;
		// TODO Auto-generated method stub
		return;
	}
	//求未来日期距离今天还剩下的天数
	 
    @Test
    public void DateUtil4() throws ParseException {
    	SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd");
    	Date date = simpleDateFormat.parse("2020-12-18");
         int i=DateUtil.offsetDate(6);
         System.out.println(i);
    }
	/**
	 * @param i
	 * @return
	 */
	private static int offsetDate(int i) {
		// TODO Auto-generated method stub
		return 0;
	}
	//求过去日期离今天过去的天数
	@Test
    public void DateUtil5() throws ParseException {
    	SimpleDateFormat simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd");
    	Date date = simpleDateFormat.parse("2020-12-18");
         int i=DateUtil.offsetDate(6);
         System.out.println(i);
    }
}
